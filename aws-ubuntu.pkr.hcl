packer {
  required_plugins {
    amazon = {
      version = ">= 1.2.8"
      source  = "github.com/hashicorp/amazon"
    }
  }
}

source "amazon-ebs" "ubuntu" {
  ami_name      = "learn-packer-linux-aws-with-nginx"
  instance_type = "t2.micro"
  region        = "eu-west-1"
  source_ami    = "ami-0905a3c97561e0b69"
  ssh_username  = "ubuntu"
}

build {
  name = "learn-packer"
  sources = [
    "source.amazon-ebs.ubuntu"
  ]


  provisioner "shell" {
    environment_vars = [
      "FOO=hello world",
    ]
    inline = [
      
        "sudo apt update",
        "sudo apt install -y nginx", 
        "sudo systemctl start nginx",
        "sudo systemctl enable nginx",
      "sudo sh -c 'echo \"<html><head><title>Welcome to Nginx!</title></head><body><h1>Hello from Nginx!</h1></body></html>\" > /var/www/html/index.html'",
        "sudo systemctl restart nginx",
    ]
  }


}